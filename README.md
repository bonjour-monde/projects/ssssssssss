**SssSssSssS**<br>
: :  LISAA Graphisme<br>
: :  02-06 janvier 2023<br>
<br><br>
![screenshot](image-ws.png)
<br><br>
————————————<br>
**CONTEXTE**<br>
L’histoire de la création typographique est une succession d’innovations techniques et d’explorations des possibilités offertes par chacune. De la mise en place des caractères mobiles à l’apparition des ordinateurs personnels, les lettres gagnent en souplesse. La dernière de ces innovations est l’invention du format de fontes variables, un nouveau type de fichier informatique qui permet d’encapsuler un grand nombre de styles typographiques et par là de les manipuler de manière très précise.

<br><br>
————————————<br>
**SUJET**<br>
Lors de cet atelier, nous explorerons comment mettre cette nouvelle technologie au service d’une création typographique expressive et expérimentale. Il sera demandé à chacun.e de réaliser un lettrage variable inspiré par un morceau de musique, qui s’animera ensuite au son de ce même morceau. Nous verrons pour cela comment dessiner des lettres vectorielles pour que celles-ci puissent être exportées au format de fonte variable, puis intégrées dans un navigateur web afin de réagir de manière générative au microphone de l’ordinateur. Le rendu final prendra la forme d’une playlist ‘pirate’ de typographies dansantes.

<br><br>
————————————<br>
**C A L E N D R I E R**<br><br>
**— Jour 1 • Lundi 02 : Introduction**<br>
**Matin**<br>
— Introduction et rencontre<br>
— Présentation du travail de Bonjour Monde<br>
— Rappel de grandes notions de typographie, présentation des fontes variables, notamment autour des standards de classification et de familles, mais aussi d’exemples allant à l’encontre ou au-delà de ces standards. Points sur les nouvelles pratiques typographiques contemporaines.<br>
— Présentation du sujet et création des groupes<br>
**Après-midi**<br>
— Collecte d’images, de typo et de mots lié à la musique choisie, recherche de rythmes graphiques et calligraphiques.<br>

**— Jour 2 • Mardi 26 : Exploration**<br>
**Matin**<br>
— Rendez-vous par groupe sur les premières propositions<br>
**Après-midi**<br>
— Introduction au dessin sur Glyphs, et à la technologie des fontes variables.
— Poursuite des explorations individuelles<br>

**— Jour 3 • Mercredi 27 : Clarification**<br>
**Matin**<br>
— Point théorique et technique<br>
— Affinage de la direction conceptuelle par groupes, au vue des explorations formelles de la veille<br>
**Après-midi**<br>
— Poursuite des explorations individuelles, dans une direction plus précise.<br>
— Mise en ligne d’images du travail en cours.

**— Jour 4 • Jeudi 28 : Finition**<br>
**Matin**<br>
— Regard collectif<br>
— Affinage de la direction par groupes<br>
**Après-midi**<br>
— Travail individuel dans une direction précise, jusqu’à obtention d’un résultat satisfaisant<br>
— Choix défini des pistes et mise en ligne d’images du travail en cours.

**— Jour 5 • Vendredi : Restitution**<br>
**Matin**<br>
— Finitions formelles et génération des caractères typographiques<br>
— Rassemblement des recherches et références pour le spécimen typographique<br>
— Rédaction d’un court paragraphe explicitant la structure et les usages particuliers à chaque famille typographique<br>
**Après-midi**<br>
— Mise en commun, génération du spécimen typographique en ligne et restitution collective
<br><br>
————————————<br>
**O U T I L S**

• [Dessin Typo] [Glyphs](https://updates.glyphsapp.com/latest3.php)<br>
• [Process] [Test fonte variables](https://fontgauntlet.com/)<br>
<br><br>
————————————<br>
**L I E N S**

• [V-Fonts](https://v-fonts.com/)<br>
• [AxisPraxis](https://www.axis-praxis.org/specimens/__DEFAULT__)<br>
• [Tutoriels pour le logiciel Glyphs](https://glyphsapp.com/learn)<br>
• [Oh No Type Co. blog](https://ohnotype.co/blog/tagged/teaching)<br>
• [Vieux spécimens typographiques numérisés (rassemblés par Emmanuel Besse)](https://www.are.na/emmanuel-besse/type-specimens-taleuy1p7xc)<br>
• [Spécimens typographiques web](https://typespecimens.xyz/specimens/all/)<br>
• [Type Review Journal](https://fontreviewjournal.com/)<br>
• [Fonts In Use](https://fontsinuse.com/)<br>
• [Modules et anatomie (par Studio Triple)](https://gitlab.com/StudioTriple/vite_et_mal/-/raw/master/documentation/01%20modules%20et%20anatomie.pdf)
<br><br>
————————————<br>
**R É F É R E N C E S**

• [Instant](https://www.poem-editions.com/products/instant)<br>
• [GlyphWorld](https://femme-type.com/a-typeface-of-nine-landscapes-glyph-world/)<br>
• [Karloff](https://www.typotheque.com/articles/beauty_and_ugliness_in_type_design)<br>
• [Wind](https://www.typotheque.com/fonts/wind)<br>
• [Stratégies italiques](http://strategiesitaliques.fr/)<br>
• [About the typefaces not used in this edition](https://www.theguardian.com/books/2002/dec/07/guardianfirstbookaward2002.gurardianfirstbookaward)<br>

